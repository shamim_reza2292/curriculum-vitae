import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, NgForm, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import { AuthService } from 'src/app/services/auth/auth.service';
import { DataService } from 'src/app/services/data.service';
import { DateTimeService } from 'src/app/services/date-time.service';
import { ModalComponent } from 'src/app/shared/modal/modal.component';

@Component({
  selector: 'app-work-experience',
  templateUrl: './work-experience.component.html',
  styleUrls: ['./work-experience.component.scss']
})
export class WorkExperienceComponent implements OnInit {
  workingExperiences: any;
  @ViewChild('workExperienceModal') workExperienceModal: ModalComponent;
  workExperienceForm: FormGroup;

  workExperienceObj ={
    id: null,
    companyName: null,
    designation: null,
    responsibilities: null,
    startDate: null,
    endDate: null,
    continuing: null    
  }
  isCountinueWork: boolean = false;
  subscriber: Subscription[] = [];
  isLoginUser: boolean = false;

  constructor(
    private authService: AuthService,
    public  dateTimeService: DateTimeService,
    private fb: FormBuilder, 
    private dataService: DataService,
    // private subscription: Subscription
    ) { }

  ngOnInit(): void {
    // this.authService.getUerInfo('workingexperience').subscribe(data=>{
    //   console.log(data);
    //   this.workingExperiences = data['data'];
      
    // });

    this.dataService.getWorkExperience();
    
   
    this.workExperienceForm = this.fb.group({
      companyName: [null, [Validators.required]],
      designation: [null, [Validators.required]],
      responsibilities: [null, [Validators.required]],
      startDate: [null, [Validators.required]],
      endDate: [null],
      continuing: [null],
    });

    this.onSubscription();

  }

  ngOnDestroy() {
    this.unSubscription()
  }


  onSubscription(){
    let onPostWorkExperience = this.dataService.onPostWorkExperience.subscribe(data=>{
      this.workExperienceModal.close();
    });
    this.subscriber.push(onPostWorkExperience);

    let ongetWorkExperience = this.dataService.ongetWorkExperience.subscribe(experienceData=>{
      this.workingExperiences = experienceData['data']; 
    });
    this.subscriber.push(ongetWorkExperience);
    const loggedIn = this.authService.onloggedIn.subscribe(logedIn=>{
      if(logedIn)
        this.isLoginUser = true;
      else
        this.isLoginUser = false;
    });
    this.subscriber.push(loggedIn);


  }

  unSubscription(){
    this.subscriber.forEach((sub: Subscription) => {
        sub.unsubscribe();
    });
  }


  onworkExperienceFormSubmit(form: NgForm){
    if(this.workExperienceForm.invalid) return;
    this.workExperienceObj.companyName = form['companyName']
    if(form['continuing'] == true){
      this.workExperienceObj.continuing = form['continuing'];
      this.workExperienceObj.endDate = null;
    }else{
      this.workExperienceObj.endDate = form['endDate']
      this.workExperienceObj.continuing = false;
    }
    this.workExperienceObj.designation = form['designation']
    this.workExperienceObj.responsibilities = form['responsibilities']
    this.workExperienceObj.startDate = form['startDate']
    console.log(this.workExperienceObj);
    

    this.dataService.postWorkExperience(this.workExperienceObj);

  }




  openEditWorkExperienceModal(experience){
    this.openWorkExperienceModal();
    this.workExperienceObj.id = experience._id;
    this.workExperienceForm.controls['companyName'].setValue(experience.companyName);
    this.workExperienceForm.controls['designation'].setValue(experience.designation);
    this.workExperienceForm.controls['responsibilities'].setValue(experience.responsibilities);
    let startDate = this.dateTimeService.formDateFormatYYMMDD(experience.startDate)
    this.workExperienceForm.controls['startDate'].setValue(startDate);
    if(experience.continuing){
      this.workExperienceForm.controls['continuing'].setValue(experience.continuing);
      this.isCountinueWork = true;
    }else{
      let endDate = this.dateTimeService.formDateFormatYYMMDD(experience.endDate);
      this.workExperienceForm.controls['endDate'].setValue(endDate);
      this.isCountinueWork = false;
    }

  }

  resetSkilForm(){
    this.workExperienceForm.controls['companyName'].setValue(null);
    this.workExperienceForm.controls['designation'].setValue(null);
    this.workExperienceForm.controls['responsibilities'].setValue(null);
    this.workExperienceForm.controls['startDate'].setValue(null);
    this.workExperienceForm.controls['endDate'].setValue(null);
    this.workExperienceForm.controls['continuing'].setValue(null);

    this.workExperienceObj.id = null;
    this.workExperienceObj.companyName = null;
    this.workExperienceObj.continuing = null;
    this.workExperienceObj.designation = null;
    this.workExperienceObj.endDate = null;
    this.workExperienceObj.responsibilities = null;
    this.workExperienceObj.startDate = null;
  }

  onContinueCheckBoxClick(event){
    console.log(event.target.checked);
    this.isCountinueWork = event.target.checked ?  true : false;
    
  }

  openWorkExperienceModal(){
    this.workExperienceModal.open();
  }

  closeWorkExperienceModal(){
    this.workExperienceModal.close();
    this.resetSkilForm();
  }

}

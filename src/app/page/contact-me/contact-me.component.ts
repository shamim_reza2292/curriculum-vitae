import { Component, ElementRef, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, NgForm, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import { AuthService } from 'src/app/services/auth/auth.service';
import { DataService } from 'src/app/services/data.service';
import { ModalComponent } from 'src/app/shared/modal/modal.component';

@Component({
  selector: 'app-contact-me',
  templateUrl: './contact-me.component.html',
  styleUrls: ['./contact-me.component.scss']
})
export class ContactMeComponent implements OnInit, OnDestroy {
  InfoData: any[] = [];
  isLoginUser: boolean ;
  contactMeFrom:FormGroup;  
 
  subscribe :Subscription[] =  [];
  @ViewChild('contactModal') contactModal: ModalComponent
  @ViewChild('demoModal') demoModal: ModalComponent


  personalObj = {
    name: null,
    phoneNumbeer: null,
    description: null,
    email: null,
    address: null,
    socialLink: null,
    designation: null
     
  }

  constructor(
    private authService: AuthService,
    private dataService: DataService,
    private fb: FormBuilder
  ) { }

  ngOnInit() {
    // use for show edit button after reload browser
    // if(this.authService.userId){
    //   this.isLoginUser = true;
    //   console.log(this.isLoginUser);      
    // }

    this.dataService.getContactMe();

    this.contactMeFrom = this.fb.group({
      name: [null, [Validators.required]],
      phoneNumbeer: [null, Validators.required],
      description: [null, Validators.required],
      email: [null, Validators.required],
      address: [null, Validators.required],
      socialLink: [null, Validators.required],
      designation: [null, Validators.required]
    })
    
    // this.authService.getUerInfo('personalInfo').subscribe(data=>{
    //   console.log(data);
    //   this.InfoData = data['data'];      
    // });

    this.onSubscribe();

    // this.authService.getToken();
  }

  ngOnDestroy(){

    this.subscribe.forEach(sub=>{
      sub.unsubscribe();
    })

  }

  onSubscribe(){

    let contactMeData = this.dataService.oncontactMe.subscribe(data=>{
      if(!data) return;
      this.InfoData = data['data']; 
      this.contactModal.close();
    })
    this.subscribe.push(contactMeData)

    const loggedIn = this.authService.onloggedIn.subscribe(logedIn=>{
      if(logedIn)
        this.isLoginUser = true;
      else
        this.isLoginUser = false;
    });

    

    this.subscribe.push(loggedIn)



  }


  contactMeSubmit(form: NgForm){

    if(this.contactMeFrom.invalid){
      return;
    }


    this.personalObj.name = form['name'];
    this.personalObj.phoneNumbeer = form['phoneNumbeer'];
    this.personalObj.description = form['description'];
    this.personalObj.email = form['email'];
    this.personalObj.address = form['address'];
    this.personalObj.socialLink = form['socialLink'];
    this.personalObj.designation = form['designation'];

    this.dataService.postContactMe(this.personalObj);
    
  } 

  resetContactForm(){
    this.contactMeFrom.controls['name'].setValue('');
    this.contactMeFrom.controls['phoneNumbeer'].setValue('');
    this.contactMeFrom.controls['description'].setValue('');
    this.contactMeFrom.controls['email'].setValue('');
    this.contactMeFrom.controls['address'].setValue('');
    this.contactMeFrom.controls['socialLink'].setValue('');
    this.contactMeFrom.controls['designation'].setValue('');
  }



  // modal open
  openModal(){
    this.contactModal.open();

    if(this.InfoData.length > 0){
    // this.contactMeFrom.value.name = this.InfoData[0]['name'];
    this.contactMeFrom.controls['name'].setValue(this.InfoData[0]['name']);
    this.contactMeFrom.controls['phoneNumbeer'].setValue(this.InfoData[0]['phoneNumbeer']);
    this.contactMeFrom.controls['description'].setValue(this.InfoData[0]['description']);
    this.contactMeFrom.controls['email'].setValue(this.InfoData[0]['email']);
    this.contactMeFrom.controls['address'].setValue(this.InfoData[0]['address']);
    this.contactMeFrom.controls['socialLink'].setValue(this.InfoData[0]['socialLink']);
    this.contactMeFrom.controls['designation'].setValue(this.InfoData[0]['designation']);
    }
  }

  closeModal(){
    this.resetContactForm();
    this.contactModal.close();
  }
  opendemoModal(){
    this.demoModal.open();
  }

  closedemoModal(){
    this.demoModal.close();
  }

}

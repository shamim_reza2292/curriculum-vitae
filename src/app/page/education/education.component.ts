import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, NgForm, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import { AuthService } from 'src/app/services/auth/auth.service';
import { DataService } from 'src/app/services/data.service';
import { ModalComponent } from 'src/app/shared/modal/modal.component';

@Component({
  selector: 'app-education',
  templateUrl: './education.component.html',
  styleUrls: ['./education.component.scss']
})
export class EducationComponent implements OnInit, OnDestroy {
  public educations: any[] = [];

  educationObj = {
    id: null,
    nameOfDegree: null,
    nameOfInstitute: null,
    passingYear: null
  }

  educationFrom: FormGroup;
  subscrib: Subscription[] = [];


  @ViewChild('educationModal') educationModal: ModalComponent;
  isLoginUser: boolean = false;

  constructor(
    private authService: AuthService,
    private fb: FormBuilder,
    private dataService: DataService
    ) { }

  ngOnInit(): void {
    // this.authService.getUerInfo('educations').subscribe(data=>{
    //   console.log(data);
    //   this.educations = data['data'];
      
    // });

    this.dataService.getEducation();

    this.educationFrom = this.fb.group({
      nameOfDegree: [null, [Validators.required]],
      nameOfInstitute: [null, [Validators.required]],
      passingYear: [null, [Validators.required]]
    });

    this.subscription();

  }

  ngOnDestroy(){
    this.unSubscrib();
  }



  subscription(){
    let onEducationPost = this.dataService.onEducationPost.subscribe(data=>{
      // this.skilsData = data['data'];
      this.educationModal.close();
    });
    this.subscrib.push(onEducationPost);

    let onGetEducation = this.dataService.onGetEducation.subscribe(data=>{
      this.educations = data['data']
    });
    this.subscrib.push(onGetEducation);

    const loggedIn = this.authService.onloggedIn.subscribe(logedIn=>{
      if(logedIn)
        this.isLoginUser = true;
      else
        this.isLoginUser = false;
    });

    this.subscrib.push(loggedIn);


  }


  unSubscrib(){
    this.subscrib.forEach((sub: Subscription)=>{
      sub.unsubscribe();
    })
  }







  onEducationFormSubmit(form: NgForm){
    if(this.educationFrom.invalid) return;
    this.educationObj.nameOfDegree = form['nameOfDegree'];
    this.educationObj.nameOfInstitute = form['nameOfInstitute'];
    this.educationObj.passingYear = form['passingYear'];

    // api

    this.dataService.postEducation(this.educationObj);

  }



  onEditEducation(eduItem){
    if(!eduItem) return;
        this.educationObj.id = eduItem._id;
        this.educationFrom.controls['nameOfDegree'].setValue(eduItem.nameOfDegree);
        this.educationFrom.controls['nameOfInstitute'].setValue(eduItem.nameOfInstitute);
        this.educationFrom.controls['passingYear'].setValue(eduItem.passingYear);
        this.openEducationModal();
  }


  resetEducationForm(){
    this.educationFrom.controls['nameOfDegree'].setValue(null);
    this.educationFrom.controls['nameOfInstitute'].setValue(null);
    this.educationFrom.controls['passingYear'].setValue(null);

    this.educationObj.id = null;
    this.educationObj.nameOfDegree = null;
    this.educationObj.nameOfInstitute = null;
    this.educationObj.passingYear = null;

  }


  openEducationModal(){
    this.educationModal.open();
  }

  closeEducationModal(){
    this.educationModal.close();
    this.resetEducationForm();
  }


} 

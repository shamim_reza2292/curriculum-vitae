import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NameAndDesignationComponent } from './name-and-designation.component';

describe('NameAndDesignationComponent', () => {
  let component: NameAndDesignationComponent;
  let fixture: ComponentFixture<NameAndDesignationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NameAndDesignationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NameAndDesignationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, NgForm, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import { AuthService } from 'src/app/services/auth/auth.service';
import { DataService } from 'src/app/services/data.service';
import { ModalComponent } from 'src/app/shared/modal/modal.component';

@Component({
  selector: 'app-skil',
  templateUrl: './skil.component.html',
  styleUrls: ['./skil.component.css']
})
export class SkilComponent implements OnInit, OnDestroy {
  skilsData: any;
  skilFrom: FormGroup;

  skilObj={
    id: null,
    skilName: null,
    confidentLavel: null
  }

  @ViewChild('skilModal') skilModal: ModalComponent;
  subscrib: Subscription[] = [];
  isLoginUser: boolean = false;

  constructor(
    private authService: AuthService,
    private fb: FormBuilder,
    private dataService: DataService
    ) { }

  ngOnInit(): void {
    // this.authService.getUerInfo('skil').subscribe(data=>{
    //   console.log(data);
    //   this.skilsData = data['data'];
      
    // });
    this.dataService.getSkil();

    this.skilFrom = this.fb.group({
      skilName: [null, [Validators.required]],
      confidentLavel: [null, [Validators.required]]
    });

    this.subscription();
  }

  ngOnDestroy(){
    this.unSubscrib();
  }

  subscription(){
    let onSkilSub = this.dataService.onSkil.subscribe(data=>{
      // this.skilsData = data['data'];
      this.skilModal.close();
    });
    this.subscrib.push(onSkilSub);
    let ongetSkil = this.dataService.onGetSkil.subscribe(data=>{
      this.skilsData = data['data']
    });
    this.subscrib.push(ongetSkil);

    const loggedIn = this.authService.onloggedIn.subscribe(logedIn=>{
      if(logedIn)
        this.isLoginUser = true;
      else
        this.isLoginUser = false;
    });
    this.subscrib.push(loggedIn);

  }


  unSubscrib(){
    this.subscrib.forEach((sub: Subscription)=>{
      sub.unsubscribe();
    })
  }

  onSkilFormSubmit(form: NgForm){
    if(this.skilFrom.invalid) return;
    this.skilObj.skilName = form['skilName'];
    this.skilObj.confidentLavel = form['confidentLavel'];
    // call api function

    this.dataService.postSkil(this.skilObj);
  }




 


onEditSkil(skilItem){
  if(!skilItem) return;
      this.skilObj.id = skilItem._id;
      this.skilFrom.controls['skilName'].setValue(skilItem.skilName);
      this.skilFrom.controls['confidentLavel'].setValue(skilItem.confidentLavel);
      this.openSkilModal();
}

resetSkilForm(){
  this.skilFrom.controls['skilName'].setValue(null);
  this.skilFrom.controls['confidentLavel'].setValue(null);
  this.skilObj.id = null;
  this.skilObj.skilName = null;
  this.skilObj.confidentLavel = null;
}






  openSkilModal(){
    this.skilModal.open();
    // if(this.skilsData.length > 0){
    //   this.skilFrom.controls['skilName'].setValue(this.skilsData[0].skilName);
    //   this.skilFrom.controls['confidentLavel'].setValue(this.skilsData[0].confidentLavel);
    // }
  }






  closeSkilModal(){
    this.resetSkilForm();
    this.skilModal.close();
  }






}

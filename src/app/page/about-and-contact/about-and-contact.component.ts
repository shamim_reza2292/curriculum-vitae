import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth/auth.service';

@Component({
  selector: 'app-about-and-contact',
  templateUrl: './about-and-contact.component.html',
  styleUrls: ['./about-and-contact.component.css']
})
export class AboutAndContactComponent implements OnInit {

  constructor(private authService: AuthService) { }


  public InfoData: any[] = [];

  ngOnInit(): void {
    // this.http.get('http://localhost:4200/api/getPersonalInfo').subscribe(data=>{
    //   console.log(data);
      
    // })

    this.authService.getUerInfo('personalInfo').subscribe(data=>{
      console.log(data);
      this.InfoData = data['data'];
      
    })

  }
  
getdata(){
  // fatch('localhost:4000/api/getPersonalInfo')
}

}

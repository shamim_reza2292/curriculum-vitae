import { Injectable } from '@angular/core';
import * as data from '../../../assets/userData/user.json'
import { Observable, Subject } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import * as mongodb from '../../../../node_modules/mongodb'

@Injectable({
  providedIn: 'root'
})
export class AuthService {



  baseUrl = 'http://localhost:4000/api/'
  // baseUrl = 'https://cvbackendapi.herokuapp.com/api/'
  storeToken = new Subject<any>();
  onStoreToken = this.storeToken.asObservable();
  userData: {};
  options: {} = {
    headers: {'content-type': 'application/json'},
    observe: 'body',
    // reportProgress: boolean,
    responseType: 'json',
    // withCredentials?: boolean,
  };

  isLoggedIn = new Subject<any>();
  onloggedIn = this.isLoggedIn.asObservable();
  userId: any;
   



  constructor(private http: HttpClient,
    ) {
    // this.getUerInfo();
      // console.log(mongodb);    
   }


   getUerInfo<Response>(url){
     return this.http.get<Response>(this.baseUrl + url, this.options);
   }



 
  //  login(email, password): Observable<any>{
  //    console.log(this.userData);        
  //    if(email == this.userData[0].email && password == this.userData[0].password){
  //      console.log('carect user');
  //       const token = localStorage.setItem('token', this.userData[0].token);
  //       this.storeToken.next(this.userData[0].token)

  //       return this.userData[0].token;
  //    }
  //  }
 
   
// local post not possible in angular Httpclient. For post need a server.
  //  postUser(email, pass){
  //    this.http.post('./assets/userData/user.json', {}, this.options ).subscribe(user=>{
  //       console.log(user);
        
  //    }),(err=>{
  //       console.log(err);
  //       throw err;
        
  //    })
  //  }


  loginWithJwt(url, loginData){
    return this.http.post<Response>(this.baseUrl + url, loginData, this.options).subscribe(loggedInData=>{
      console.log(loggedInData);
      if(!loggedInData) return;
      this.isLoggedIn.next(loggedInData['user'].id);
      // this.userId = loggedInData['user'].id;
      // window.sessionStorage.setItem( 'token' , loggedInData['token']);
      // this.setToken(loggedInData);
      // this.setRefrshToken(loggedInData['referencehToken'])
      



    });
  }


  isLogedIn(){
    // return this.getToken();
  }


  setToken(token){
    localStorage.setItem('access_token', token['accessToken']);
    localStorage.setItem('referencehToken', token['referencehToken']);
  }

  getToken(){
    return localStorage.getItem('access_token');
  }

  
// refresh token
  refreshToken(){
    let refreshTokenBody = this.getRefrshToken();
    this.http.post(`${this.baseUrl}refresToken`, refreshTokenBody, this.options).subscribe( loggedInData =>{
      this.isLoggedIn.next(loggedInData['user'].id);
    }, (err)=>{
      this.logout();
    })
  }
  setRefrshToken(refrshToken: string){
    return localStorage.setItem('referencehToken', refrshToken);
  }

  getRefrshToken(){
    return localStorage.getItem('referencehToken');
  }

  logout(){
    
    // location.reload();

    this.http.get(`${this.baseUrl}logout`, this.options).subscribe(data=>{
      // location.reload();
      this.isLoggedIn.next(null);
    },err=>{
      
    })

  }



}

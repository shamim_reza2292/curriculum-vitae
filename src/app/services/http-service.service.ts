import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class HttpServiceService {

  baseUrl = 'http://localhost:4000/api/'
  // baseUrl = 'https://cvbackendapi.herokuapp.com/api/'

  options: {} = {
    headers: {'content-type': 'application/json'},
    observe: 'body',
    // reportProgress: boolean,
    responseType: 'json',
    // withCredentials?: boolean,
  };

  constructor(private http: HttpClient,) { }



  get<Response>(url){
    return this.http.get<Response>(this.baseUrl + url, this.options);
  }

  post<Response>(url, body){
    return this.http.post<Response>(this.baseUrl + url, body, this.options);
  }









}

import { Component, OnInit, ViewChild, ElementRef, Renderer2, OnDestroy } from '@angular/core';
import * as data from '../assets/userData/user.json'
import { FormBuilder, NgForm } from '@angular/forms';
import { AuthService } from './services/auth/auth.service';
import { ModalComponent } from './shared/modal/modal.component';
import { Subscription } from 'rxjs';

import html2canvas from "html2canvas";
import jsPDF from 'jspdf'

// declare const html2canvas: any;
// declare const jsPDF: any;
// (window as any).html2canvas = html2canvas;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, OnDestroy {
  title = 'curriculum-vitae';
  isToken: boolean;
  // isEditMode: boolean = true;

  loginForm;
  subscription: Subscription[] = [];

  @ViewChild('clossButton', {static: false}) clossButton: ElementRef;
  @ViewChild('cvBody', {static: false}) cvBody: ElementRef;
  @ViewChild('loginModal') loginModal: ModalComponent;
  @ViewChild('DownloadModal') DownloadModal: ModalComponent;

  constructor(
    private fb: FormBuilder,
    private authService: AuthService,
    private renderer: Renderer2,
    private elm: ElementRef
    ){

  }

   ngOnInit(){
     
    //  console.log(data);
    // localStorage.removeItem('token')

     this.loginForm = this.fb.group({
      "email": [null],
      "password": [null]
     })
    //  console.log(this.loginForm);     

    // this.loginModal.nativeElement.style.display = 'none';
    // let onStoreToken = this.authService.onStoreToken.subscribe(tokenData=>{
    //   console.log(tokenData);
    //   this.isToken = tokenData? true : false;
    //   this.clossButton.nativeElement.click();
    //   this.isEditMode = false;
    // })

    this.subscribe();

   }

   ngOnDestroy(){
    this.unsubscribe();
   }


   subscribe(){
    let loninComplete = this.authService.onloggedIn.subscribe(data=>{
        this.loginModal.close()
    })
    this.subscription.push(loninComplete);
   }


  unsubscribe(){
    this.subscription.forEach((sub: Subscription) => {
      sub.unsubscribe();
    });
  }



   loginSubmit(form: NgForm){
      console.log(form);   
      
      // this.authService.login(form['userEmail'], form['userPassword']);

      // this.authService.postUser(form['userEmail'], form['userPassword']);
      
   }


   lonin(form: NgForm){
    console.log(form);   

    let userInfo ={
      email: form['email'],
      password: form['password']
    }

 


    this.authService.loginWithJwt('login', userInfo);



   }


   onLogoutButtonClick(){
     this.authService.logout();
   }


   openLoginModal(){
    this.loginModal.open();
   }
   closeLoginModal(){
     this.loginModal.close();
   }



   openDownloadModal(){
    this.DownloadModal.open();
   }


   closeDownloadModal(){
    this.DownloadModal.close();
   }

   onDownloadOkeyBtnClick(){
    // this.cvBody.nativeElement
    // let cvBody = document.getElementById("cvBody");
    
     

  //   html2canvas(this.cvBody.nativeElement).then( (canvas) => {
  //     let fileWidth = 208;
  //     let fileHeight = (canvas.height * fileWidth) / canvas.width;
  //     var FILEURI = canvas.toDataURL("image/png");
  //     var doc = new jsPDF("p","mm","a4");
  //     let position = 0;
  //     // doc.addImage(img, 'JPEG', 0, 0, 20, 20);
  //     doc.addImage(FILEURI, 'JPEG', 0, 0, fileWidth, fileHeight)  
  //     doc.save('cv.pdf');
      
  // });

    let PDF = new jsPDF("p","mm","a4");
    PDF.html(this.cvBody.nativeElement,   
    {
      callback:  (doc)=> {
        // doc.save();
        doc.output('dataurlnewwindow');

      },
      // callback: (doc: jsPDF) => { },
    margin: [0 , 0, 20, 0],
    autoPaging: true, // boolean | "slice" | "text"
    // filename?: string,
    // image: HTMLOptionImage,
    // html2canvas?: Html2CanvasOptions,
    // jsPDF?: jsPDF,
    x: 0,
    y: 0,
    width: 208,
    windowWidth: 800,
    // fontFaces?: HTMLFontFace[];
  }

  )

    // let fileWidth = 208;

    //let fileHeight = (canvas.height * fileWidth) / canvas.width;
     
   this.DownloadModal.close();

  }

  
}   
